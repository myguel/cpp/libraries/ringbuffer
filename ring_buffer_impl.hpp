#pragma once

#include "ring_buffer.hpp"
#include <algorithm>

namespace stg {

#ifndef TEMPLATEM
#define TEMPLATEM template <class T, size_t SIZE>
#else
#error alias TEMPLATEM all ready defined!
#endif //< TEMPLATEM

#ifndef CLASSM
#define CLASSM RingBuffer<T, SIZE>
#else
#error alias CLASSM all ready defined!
#endif //< CLASSM

TEMPLATEM
inline CLASSM::RingBuffer()
    : m_idx(0)
    , m_size(0)
{
}

TEMPLATEM
CLASSM::RingBuffer(const T &default_elem)
    : m_idx(0)
    , m_size(0)
{
    for (size_t i = 0; i < capacity(); ++i)
    {
        m_ring[i] = default_elem;
    }
}

TEMPLATEM
void CLASSM::init()
{
    m_idx = 0;
    m_size = 0;
}

TEMPLATEM
void CLASSM::init(const T &default_elem)
{
    m_idx = 0;
    m_size = 0;

    for (size_t i = 0; i < capacity(); ++i)
    {
        m_ring[i] = default_elem;
    }
}

TEMPLATEM
inline size_t CLASSM::capacity() const
{
    return SIZE;
}

TEMPLATEM
inline size_t CLASSM::size() const
{
    return m_size;
}

TEMPLATEM
inline void CLASSM::push(const T &elem)
{
    m_ring[m_idx] = elem;
    m_size = std::min(m_idx + 1, capacity());
    roll();
}

TEMPLATEM
inline const T &CLASSM::last(size_t n) const
{
    size_t data_index = (m_idx-1-n) % capacity();
    return m_ring[data_index];
}

TEMPLATEM
inline T &CLASSM::last(size_t n)
{
    size_t data_index = (m_idx-1-n) % capacity();
    return m_ring[data_index];
}

TEMPLATEM
inline const T &CLASSM::next(size_t n) const
{
    size_t data_index = (m_idx + n) % capacity();
    return m_ring[data_index];
}

TEMPLATEM
inline T &CLASSM::next(size_t n)
{
    size_t data_index = (m_idx + n) % capacity();
    return m_ring[data_index];
}

TEMPLATEM
inline T &CLASSM::e()
{
    return m_ring[m_idx];
}

TEMPLATEM
inline const T &CLASSM::e() const
{
    return m_ring[m_idx];
}

TEMPLATEM
inline void CLASSM::roll()
{
    m_idx = (m_idx + 1) % capacity();
}

#undef CLASSM
#undef TEMPLATEM

} // namespace stg