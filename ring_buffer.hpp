#pragma once

#include <stddef.h>

namespace stg
{

/** @class RingBuffer
 * @brief Ring buffer with fixed size window.
 * 
 * The class has 2 template parameters: the type of the object stored in 
 * the ring buffer and the fixed size of the window. It is possible to 
 * initialize the ring buffer with some default element. The main methods
 * are ```next``` and ```push``` that allow respectively to recover the next element
 * that will be replaced in the ring buffer and to replace it with a new
 * element. The method ```e``` is a placeholder for ```next(0)```.
*/
template <class T, size_t SIZE>
class RingBuffer
{
public:
  RingBuffer();

  /**
     * @brief Constructor filling the buffer with the element default_elem.
     * */
  RingBuffer(const T &default_elem);

  /**
     * @brief Reset all indices to 0. Size is 0 after an init call.
     */
  void init();

  /**
     * @brief Initialize the buffer with the element default_elem and reset all indices to 0. Size is 0 after an init call.
     * */
  void init(const T &default_elem);

  /**
     * @brief Returns the maximum number of elements that can be stored in the ring buffer. 
     * @return The maximum number of elements that can be stored in the ring buffer.
     * */
  size_t capacity() const;

  /**
     * @brief Returns the number of elements that have been pushed until it reaches the buffer capacity, returns the buffer capacity afterwards. 
     * @return The number of elements that have been pushed until it reaches the buffer capacity, returns the buffer capacity afterwards.
     * */
  size_t size() const;

  /**
     * @brief  Add an element at the end of the ring buffer.
     * @param elem The element to be added at the end of the ring buffer.
     * 
     * Add an element at the end of the ring buffer. If the next place is 
     * beyond the capacity of the buffer, the next place circle back to the 
     * begining of the buffer, hence the name ring buffer. So push may erase
     * a previously stored element, that will be lost.
     * If it is necessary to get the next element that will be replaced, use:
     * ```
     * next_replaced_elem = ring_buffer.next();
     * ```
     * 
     * */
  void push(const T &elem);

  /**
     * @brief  Get the n-th element that have been inserted in the ring buffer counting from the last.
     * 
     * Get the n-th element that have been inserted in the ring buffer counting from the last. If ```n is > ring.size()``` the returned 
     * element is not necessary an element that has been inserted. It returns
     * what is contained in the ring buffer at the n-th position counting 
     * from the last inserted element modulo the buffer capacity.
     * 
     * @param n The position in history: 0 for the last element inserted, 
     * 1 for the element before the last, 2 for the element before the element 
     * before the last, etc.
     * @return The n-th element that have been inserted in the ring buffer counting from the last.
     */
  const T &last(size_t n = 0) const;

  /**
     * @brief  Get the n-th element that have been inserted in the ring buffer counting from the last.
     * 
     * Get the n-th element that have been inserted in the ring buffer counting from the last. If ```n is > ring.size()``` the returned 
     * element is not necessary an element that has been inserted. Is returned
     * what is contained in the ring buffer at the n-th position counting 
     * from the last inserted element modulo the buffer capacity.
     * 
     * @param n The position in history: 0 for the last element inserted, 
     * 1 for the element before the last, 2 for the element before the element 
     * before the last, etc.
     * @return The n-th element that have been inserted in the ring buffer counting from the last.
     */
  T &last(size_t n = 0);

  /**
     * @brief  Get the n-th element that will been replaced in the ring buffer counting from the next to be replaced.
     * @param n The order in replacement: 0 for the next element to be 
     * replaced, 1 for the element after the next, 2 for the element after the
     * element after the next element to be replaced, etc.
     * @return The element that will be replaced after n insertions.
     */
  const T &next(size_t n = 0) const;

  /**
     * @brief  Get the n-th element that will been replaced in the ring buffer counting from the next to be replaced.
     * @param n The order in replacement: 0 for the next element to be 
     * replaced, 1 for the element after the next, 2 for the element after the
     * element after the next element to be replaced, etc.
     * @return The element that will be replaced after n insertions.
     */
  T &next(size_t n = 0);

  /**
     * @brief  Get the current element. The one that will be replaced if a push would be called. 
     * 
     * This is equivalent to ``next(0)``
     * ```
     * ring.e() == ring.next(0)
     * ```
     * @return The element that will be replaced after 1 insertion.
     */
  T &e();

  /**
     * @brief  Get the current element. The one that will be replaced if a push would be called.
     * 
     * This is equivalent to ``next(0)``
     * ```
     * ring.e() == ring.next(0)
     * ```
     * @return The element that will be replaced after 1 insertion.
     */
  const T &e() const;

  /**
     * @brief Increment the position in the ring buffer.
     */
  void roll();

protected:
  size_t m_idx;
  size_t m_size;
  T m_ring[SIZE];
};

} // namespace stg

#include "ring_buffer_impl.hpp"
