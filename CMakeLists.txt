#This CMakeLists.txt was automatically generated.
#You can add things but not change existing things.
#For instance don't change the variable names !
#One exception: you can change the name of the the target library.

cmake_minimum_required(VERSION 2.4)

if(COMMAND cmake_policy)
	if( POLICY CMP0003 )
   cmake_policy(SET CMP0003 NEW)
	endif( POLICY CMP0003 )
	if( POLICY CMP0015 )
    cmake_policy(SET CMP0015 NEW)
  endif( POLICY CMP0015 )
endif(COMMAND cmake_policy)

project( ring_buffer )

if( NOT CMAKE_VERBOSE_MAKEFILE )
    set( CMAKE_VERBOSE_MAKEFILE ON )
endif( NOT CMAKE_VERBOSE_MAKEFILE )

#CUSTOM SETTINGS
set( CUSTOM_CMAKE_SETTINGS $ENV{CMAKE_COMPILE_VARIABLES} )

if( NOT EXISTS ${CUSTOM_CMAKE_SETTINGS} )
	message( "Warning: CMAKE_COMPILE_VARIABLES not defined, switch to default configuration." )
	set( ENV{CMAKE_COMPILE_VARIABLES} ${PROJECT_SOURCE_DIR}/CMakeCompileVars.cmake.example )
endif( NOT EXISTS ${CUSTOM_CMAKE_SETTINGS} )
message( "The environement variable CMAKE_COMPILE_VARIABLES is set to " $ENV{CMAKE_COMPILE_VARIABLES} )

include( $ENV{CMAKE_COMPILE_VARIABLES} )


if( NOT FLAGS )
	set( FLAGS "" )
endif( NOT FLAGS )
#message( "flags: " ${FLAGS} )

#C compiler
###########
set( CMAKE_C_FLAGS_DEBUG "${CC_DEBUG_FLAGS} ${FLAGS}" )
set( CMAKE_C_FLAGS_RELEASE "${CC_OPTIMIZATION_FLAGS} ${FLAGS}" )

#C++ compiler
#############
set( CMAKE_CXX_FLAGS_DEBUG "${CXX_DEBUG_FLAGS} ${FLAGS}" )
set( CMAKE_CXX_FLAGS_RELEASE "${CXX_OPTIMIZATION_FLAGS} ${FLAGS}" )

#message( "flags: " ${flags} )

if ( NOT CMAKE_BUILD_TYPE )
	set ( CMAKE_BUILD_TYPE Debug )
endif ( NOT CMAKE_BUILD_TYPE )

## Set CMAKE_MODULE_PATH which is a list
set(
  CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/CMake)

#I N C L U D E S
#include_directories( )

#L I B R A R Y   P A T H S
#link_directories( )

# L I B R A R I E S (needed by a program that uses that library)
set( ring_buffer_REQUIRED_LIBRARIES )

#L I B R A R Y
file( GLOB ring_buffer_src ${PROJECT_SOURCE_DIR}/[^~.]*.[cC] ${PROJECT_SOURCE_DIR}/[^~.]*.[cC]?? ${PROJECT_SOURCE_DIR}/[^~.]*.[cC][cC] )
file( GLOB ring_buffer_headers ${PROJECT_SOURCE_DIR}/[^~.]*.[thH] ${PROJECT_SOURCE_DIR}/[^~.]*.[thH]?? )



#QT4
set( QT4_USED FALSE )
set( ring_buffer_moc_files )
set( ring_buffer_moc_headers )

if( QT4_USED )
	qt4_find_headers( ring_buffer_moc_headers ${ring_buffer_headers} )
	find_package(Qt4 REQUIRED) # find and setup Qt4 for this project
	# the next line sets up include and link
	#directories and defines some variables that we will use.
	# you can modify the behavior by setting some variables, e.g.
	#set(QT_USE_QTOPENGL TRUE)
	# -> this will cause cmake to include and link against the OpenGL module
	include(${QT_USE_FILE})
	include_directories( ${QT_QT_INCLUDE_DIR} )
	if( ring_buffer_src )
		qt4_wrap_cpp( ring_buffer_moc_files ${ring_buffer_moc_headers} )
	endif( ring_buffer_src )
	# libraries
	set( ring_buffer_REQUIRED_LIBRARIES
		${ring_buffer_REQUIRED_LIBRARIES}
		${QT_LIBRARIES} )
endif( QT4_USED )

if( ring_buffer_src )
	add_library( ring_buffer SHARED ${ring_buffer_src} ${ring_buffer_moc_files} )
	target_link_libraries( ring_buffer ${ring_buffer_REQUIRED_LIBRARIES} )
endif( ring_buffer_src )

include_directories( ${PROJECT_SOURCE_DIR}/ )
set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/lib)

if( ring_buffer_src )
	#INSTALL(TARGETS ring_buffer DESTINATION ${LOCAL_LIBRARIES} )
	#INSTALL(FILES ${ring_buffer_headers} DESTINATION ${LOCAL_INCLUDES}/ring_buffer )
endif( ring_buffer_src )

# --- Package generation
#include ( ../CMake/Dependencies.cmake )

set ( PACKAGE_NAME ${PROJECT_NAME} )
set ( PACKAGE_VERSION 1.0.0 )
set ( PACKAGE_RELEASE 0 )
set ( PACKAGE_MAINTAINER_NAME "Manuel Yguel" )
set ( PACKAGE_MAINTAINER_EMAIL "manuel.yguel@gmail.com" )
set ( PACKAGE_DESCRIPTION_SUMMARY "" )
set ( PACKAGE_DESCRIPTION "" )
set ( PACKAGE_DEPENDS  )


# Debian
#include ( ../CMake/DpkgDeb.cmake )
#if ( DPKG_FOUND )
#    add_debian_package ( ${PROJECT_NAME} )
#endif ( DPKG_FOUND )

# RPM packages
#include ( ../CMake/Rpmbuild.cmake )
#set ( PACKAGE_GROUP "Groupe" )
#set ( PACKAGE_LICENSE "LGPL" )

#if ( RPMBUILD_FOUND )
#    add_rpm ( ${PROJECT_NAME} )
#endif ( RPMBUILD_FOUND )

#add_custom_target ( deb )
#add_custom_command ( TARGET deb
#	COMMAND ${CMAKE_MAKE_PROGRAM} ${PROJECT_NAME}_deb )
	
#add_custom_target ( rpm )
#add_custom_command ( TARGET rpm
#    COMMAND ${CMAKE_MAKE_PROGRAM} ${PROJECT_NAME}_rpm )

###########
#T E S T S
###########

#begin( LIB_TESTS )
if( BUILD_LIBRARY_TESTS )
#add_subdirectory( test/testing_is_necessary_to_provide_good_libraries )
endif( BUILD_LIBRARY_TESTS )
#end( LIB_TESTS )
